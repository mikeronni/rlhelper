from setuptools import setup

setup(name='rlhelper',
      version='0.52',
      description='Shortcut functions for canvas strings and spacing objects. Inputs must be * 72 (inch)',
      url='https://bitbucket.org/mikeronni/rlhelper',
      author='Michael Lance',
      author_email='michael.lance@gmail.com',
      license='MIT',
      packages=['rlhelper'],
      zip_safe=False)